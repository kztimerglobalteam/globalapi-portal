# Contributing

## Contribute by helping support more languages!

## How to add a new language
Add a language block at the bottom of [nuxt.config.js](./nuxt.config.js) in the `locales` block
```
i18n: {
  locales: [
    {
      code: 'en',
      file: 'en-US.js'
    },
    {
      code: 'es',
      file: 'es.js'
    }
  ],
```
Create the file in the [lang directory](./lang)

Check an existing language file such as the [english file](./lang/en-US.js) for reference

Each file should be in the same format
```javascript
export default {
  signIn: 'Sign In',
  navigationTitle: 'GlobalAPI Dashboard',
  // ...
  editBan: 'Edit Ban {id} - {steamid}'
};
```
Within your language file, keep the `signIn` part, but change `'Sign In'` to right phrase in your language.

Stuff in curly brackets such as `{id}` or `{steamid}` can be moved around, but you cannot change the content of it
