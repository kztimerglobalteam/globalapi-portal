module.exports = {
  extends: [
    'airbnb-typescript/base',
    '@nuxtjs/eslint-config-typescript'
  ],
  rules: {
    'semi': 'off',
    'max-len': 'off',
    'camelcase': 'off',
    'no-unused-vars': 'off',
    'class-methods-use-this': 'off',
    'import/no-extraneous-dependencies': 'off',
    'lines-between-class-members': ['error', 'always', { exceptAfterSingleLine: true }],
    'no-param-reassign': ['error', { props: false }],
    '@typescript-eslint/camelcase': ['error', { ignoreDestructuring: true, properties: 'never' }],
    '@typescript-eslint/no-unused-vars': ['warn']
  }
}
