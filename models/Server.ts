export default interface Server {
  id: number;
  port: number;
  ip: string;
  name: string;
  owner_steamid64: string;
}
