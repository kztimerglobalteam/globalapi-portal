export default interface Ban {
  id: number;
  ban_type: string;
  expires_on: string;
  steamid64: string;
  player_name: string;
  steam_id: string;
  notes: string;
  stats: string;
  server_id: number;
  updated_by_id: string;
  created_on: string;
  updated_on: string;
}
