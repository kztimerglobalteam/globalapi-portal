export default interface RecordFilter {
  id: number;
  map_id: number;
  stage: number;
  mode_id: number;
  tickrate: number;
  has_teleports: boolean;
  created_on: string;
  updated_on: string;
  updated_by_id: string;
}
