export default interface Jumpstat {
  id: number;
  server_id: number;
  steamid64: string;
  player_name: string;
  steam_id: string;
  jump_type: number;
  distance: number;
  tickrate: number;
  msl_count: number;
  strafe_count: number;
  is_crouch_bind: number;
  is_forward_bind: number;
  is_crouch_boost: number;
  updated_by_id: string;
  created_on: string;
  updated_on: string;
}
