export default interface Map {
  id: number;
  name: string;
  filesize: number;
  validated: boolean;
  difficulty: number;
  created_on: string;
  updated_on: string;
  approved_by_steamid64: string;
  workshop_url: string;
  download_url: string;
}
