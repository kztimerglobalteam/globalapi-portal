export default interface Mode {
  id: number;
  name: string;
  description: string;
  latest_version: number;
  latest_version_description: string;
  website: string;
  repo: string;
  contact_steamid64: string;
  supported_tickrates?: number[];
  created_on: Date;
  updated_on: Date;
  updated_by_id: string;
}