/* eslint-disable no-shadow */
/* eslint-disable no-param-reassign */
import { PORTAL_HOST } from '@/app.config';

export const state = () => ({
  loggedIn: false,
  id: '',
  token: '',
  refreshToken: '',
  expires: '',
  role: '',
  avatar: '',
  name: '',
  profile: ''
});

export const mutations = {
  setId (state, id) {
    state.id = id;
  },
  setToken (state, token) {
    state.token = token;
  },
  setRefreshToken (state, refreshToken) {
    state.refreshToken = refreshToken;
  },
  setRole (state, role) {
    state.role = role;
  },
  setExpiration (state, expires) {
    state.expires = expires;
  },
  setLoginState (state, loggedIn) {
    state.loggedIn = loggedIn;
  },
  setAvatar (state, avatar) {
    state.avatar = avatar;
  },
  setName (state, name) {
    state.name = name;
  },
  setProfile (state, url) {
    state.profile = url;
  }
};

export const getters = {
  isLoggedIn: state => state.loggedIn,
  isUser: state => state.role === 'User',
  isGlobalAdmin: state => state.role === 'GlobalAdmin',
  isMapAdmin: state => state.role === 'MapAdmin',
  getId: state => state.id,
  getAvatar: state => state.avatar,
  getName: state => state.name,
  getProfile: state => state.profile
};

export const actions = {
  async initialize ({ commit }) {
    commit('setLoginState', false);
    try {
      // eslint-disable-next-line @typescript-eslint/camelcase
      const {
        authenticated, token, refresh_token, expires, role, steam_id64, profile
      } = await this.$axios.$get(`${PORTAL_HOST}/api/v2.0/auth/info`, { withCredentials: true });
      if (!authenticated) { return; }
      commit('setToken', token);
      commit('setRefreshToken', refresh_token);
      commit('setExpiration', expires);
      commit('setRole', role);
      commit('setId', steam_id64);
      commit('setLoginState', authenticated);
      commit('setAvatar', profile.avatarmedium);
      commit('setName', profile.personaname);
      commit('setProfile', profile.profileurl);
    } catch {
      console.log('Unable to verify user');
    }
  },
  async refresh ({ state, commit }) {
    console.log('refreshing');
    const { token, refresh_token, expires } = await this.$axios.$get(`${PORTAL_HOST}/api/v2.0/auth/refresh`, {
      withCredentials: true,
      params: {
        token: state.refreshToken
      }
    });
    commit('setToken', token);
    commit('setRefreshToken', refresh_token);
    commit('setExpiration', expires);
  }
};
