/* eslint-disable no-param-reassign */
/* eslint-disable no-shadow */
export const state = () => ({
  darkTheme: true,
  relative: true
});

export const mutations = {
  setTheme (state, dark) {
    state.darkTheme = dark;
  },
  setRelative (state, useRelative) {
    state.relative = useRelative;
  }
};
