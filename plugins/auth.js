export default function ({ $axios, store }) {
  $axios.onRequest((config) => {
    if (config.withCredentials) {
      config.headers.Authorization = `Bearer ${store.state.user.token}`;
    }
  });
}
