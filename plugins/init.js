// export default async function ({ store }) {
//   await store.dispatch('user/initialize');
// }

import createAuthRefreshInterceptor from 'axios-auth-refresh';

export default async function ({ $axios, store, app }) {
  await store.dispatch('user/initialize');
  createAuthRefreshInterceptor($axios, async (_failedRequest) => {
    console.log('refreshing token');
    await store.dispatch('user/refresh');
    return Promise.resolve();
  });
}
