# globalapi-portal

> Portal for GlobalAPI

### Features (TODO list?)
- [x] Ban
  - [x] View Ban
  - [x] Add Ban
  - [x] Edit Ban (and Unban)
- [x] Map
  - [x] View Map
  - [x] Add Map
  - [x] Edit Map
- [ ] Record Filters
  - [x] View Record Filters
  - [x] Add Record Filters
  - [ ] Remove Record Filters
- [x] Server
  - [x] View Server
    - [x] Admin View
      - [x] Easy Apply+Deny
    - [x] User View
    - [x] Server List
      - [x] (Optional) Add steam connect url?
  - [x] Apply Server
- [x] Record
  - [x] View Record 
  - [x] Filter Records
  - [ ] (Optional) Delete Records
- [x] Jumpstat
  - [x] View Jumpstat
  - [x] Filter Jumpstats
  - [ ] (Optional) Delete Jumpstat
- [ ] i18n support
- [x] Unit Tests

## Initial
* Validate API_HOST location at [app.config.js](app.config.js)
```bash
yarn install
```
## DEV
```bash
yarn run dev
```

## PROD
```bash
# SPA mode (static build)
yarn generate
# Outputs to dist folder

# Lets Nuxt start their own server to serve files (port 3000)
yarn start
```


For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
