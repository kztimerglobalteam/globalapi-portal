import Vue from 'vue';
import Vuex from 'vuex';
import Vuetify from 'vuetify';
import VueTestUtils from '@vue/test-utils';
import en from 'vuetify/es5/locale/en';
import authStore from '@/store/user';
import settingStore from '@/store/setting';

Vue.config.productionTip = false;

Vue.use(Vuex);
Vue.use(Vuetify, {
  lang: {
    locales: { en },
    current: 'en'
  }
});

const store = new Vuex.Store({
  modules: {
    auth: {
      namespaced: true,
      ...authStore
    },
    setting: {
      namespaced: true,
      ...settingStore
    }
  }
});

// Mock Nuxt components
VueTestUtils.config.stubs.nuxt = '<span>nuxt</span>';
VueTestUtils.config.stubs.NuxtLink = '<span>nuxt-link</span>';
VueTestUtils.config.mocks.$store = store;
VueTestUtils.config.mocks.store = store;
