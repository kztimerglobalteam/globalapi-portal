import { shallowMount } from '@vue/test-utils';
import DefaultLayout from '@/layouts/default.vue';

const createWrapper = () => shallowMount(DefaultLayout, {
  mocks: {
    $nuxt: {
      $route: {
        path: '/dashboard/test'
      }
    }
  }
});

describe('Default Layout', () => {
  test('mounts properly', () => {
    const wrapper = createWrapper();
    expect(wrapper.isVueInstance()).toBeTruthy();
  });

  test('renders properly', () => {
    const wrapper = createWrapper();
    expect(wrapper.html()).toMatchSnapshot();
  });
});
