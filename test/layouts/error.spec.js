import { shallowMount } from '@vue/test-utils';
import ErrorLayout from '@/layouts/error.vue';

const createWrapper = () => shallowMount(ErrorLayout, {
  propsData: {
    error: {
      statusCode: 404
    }
  }
});

describe('Error Layout', () => {
  test('mounts properly', () => {
    const wrapper = createWrapper();
    expect(wrapper.isVueInstance()).toBeTruthy();
  });

  test('renders properly', () => {
    const wrapper = createWrapper();
    expect(wrapper.html()).toMatchSnapshot();
  });
});
