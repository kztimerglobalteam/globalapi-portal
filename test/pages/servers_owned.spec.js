import { shallowMount } from '@vue/test-utils';
import OwnedPage from '@/pages/dashboard/servers/owned.vue';

const createWrapper = () => shallowMount(OwnedPage, { mocks: { $axios: { $get: () => Promise.resolve([]) } } });

describe('Servers Page', () => {
  test('mounts properly', () => {
    const wrapper = createWrapper();
    expect(wrapper.isVueInstance()).toBeTruthy();
  });

  test('renders properly', () => {
    const wrapper = createWrapper();
    expect(wrapper.html()).toMatchSnapshot();
  });
});
