import { shallowMount } from '@vue/test-utils';
import Index from '@/pages/dashboard/maps/_id.vue';

const createWrapper = () => shallowMount(Index, {
  mocks: {
    $axios: { $get: () => Promise.resolve(undefined) },
    $route: {
      params: {
        id: 1234
      }
    }
  }
});

describe('Map Page', () => {
  test('mounts properly', () => {
    const wrapper = createWrapper();
    expect(wrapper.isVueInstance()).toBeTruthy();
  });

  test('renders properly', () => {
    const wrapper = createWrapper();
    expect(wrapper.html()).toMatchSnapshot();
  });
});
