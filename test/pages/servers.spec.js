import { shallowMount } from '@vue/test-utils';
import Index from '@/pages/dashboard/servers/index.vue';

const createWrapper = () => shallowMount(Index, { mocks: { $axios: { $get: () => Promise.resolve([]) } } });

describe('Servers Page', () => {
  test('mounts properly', () => {
    const wrapper = createWrapper();
    expect(wrapper.isVueInstance()).toBeTruthy();
  });

  test('renders properly', () => {
    const wrapper = createWrapper();
    expect(wrapper.html()).toMatchSnapshot();
  });
});
