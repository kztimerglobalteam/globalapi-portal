import { shallowMount } from '@vue/test-utils';
import Index from '@/pages/dashboard/records/index.vue';

const createWrapper = () => shallowMount(Index, { mocks: { $axios: { $get: () => Promise.resolve([]) } } });

describe('Records Page', () => {
  test('mounts properly', () => {
    const wrapper = createWrapper();
    expect(wrapper.isVueInstance()).toBeTruthy();
  });

  test('renders properly', () => {
    const wrapper = createWrapper();
    expect(wrapper.html()).toMatchSnapshot();
  });
});
