import { shallowMount } from '@vue/test-utils';
import Index from '@/pages/dashboard/bans/index.vue';

const createWrapper = () => shallowMount(Index, { mocks: { $axios: { $get: () => Promise.resolve([]) } } });

describe('Ban Page', () => {
  test('mounts properly', () => {
    const wrapper = createWrapper();
    expect(wrapper.isVueInstance()).toBeTruthy();
  });

  test('renders properly', () => {
    const wrapper = createWrapper();
    expect(wrapper.html()).toMatchSnapshot();
  });
});
