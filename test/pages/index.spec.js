import { shallowMount } from '@vue/test-utils';
import Index from '@/pages/index.vue';

describe('Default Page', () => {
  const router = ({
    push: () => 'go to dashboard'
  });

  test('mounts properly', () => {
    const wrapper = shallowMount(Index, { mocks: { $router: router } });
    expect(wrapper.isVueInstance()).toBeTruthy();
  });

  test('renders properly', () => {
    const wrapper = shallowMount(Index, { mocks: { $router: router } });
    expect(wrapper.html()).toMatchSnapshot();
  });
});
