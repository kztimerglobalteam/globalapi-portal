import { shallowMount } from '@vue/test-utils';
import Index from '@/pages/dashboard/index.vue';

describe('Default Dashboard Page', () => {
  test('mounts properly', () => {
    const wrapper = shallowMount(Index);
    expect(wrapper.isVueInstance()).toBeTruthy();
  });

  test('renders properly', () => {
    const wrapper = shallowMount(Index);
    expect(wrapper.html()).toMatchSnapshot();
    expect(wrapper.text()).toContain('Peepo');
  });
});
