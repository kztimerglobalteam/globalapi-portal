import { shallowMount } from '@vue/test-utils';
import RecordFilterForm from '@/components/record_filters/AddRecordFilterForm.vue';

const createWrapper = () => shallowMount(RecordFilterForm);

describe('Record Filter Form', () => {
  test('mounts properly', () => {
    const wrapper = createWrapper();
    expect(wrapper.isVueInstance()).toBeTruthy();
  });

  test('renders properly', () => {
    const wrapper = createWrapper();
    expect(wrapper.html()).toMatchSnapshot();
  });
});
