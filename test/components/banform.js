import { shallowMount } from '@vue/test-utils';
import BanForm from '@/components/bans/BanForm.vue';

const createNewBanForm = () => shallowMount(BanForm, {
  propsData: {
    value: null
  }
});

const createEditBanForm = () => shallowMount(BanForm, {
  propsData: {
    value: {
      id: 123,
      steam_id: 'test',
      type: 'type',
      notes: 'test_notes',
      stats: 'test_stats'
    }
  }
});

describe('New Ban Form', () => {
  test('mounts properly', () => {
    const wrapper = createNewBanForm();
    expect(wrapper.isVueInstance()).toBeTruthy();
  });

  test('renders properly', () => {
    const wrapper = createNewBanForm();
    expect(wrapper.html()).toMatchSnapshot();
    expect(wrapper.text()).toContain('New Ban');
  });
});

describe('Edit Ban Form', () => {
  test('mounts properly', () => {
    const wrapper = createEditBanForm();
    expect(wrapper.isVueInstance()).toBeTruthy();
  });

  test('renders properly', () => {
    const wrapper = createEditBanForm();
    expect(wrapper.html()).toMatchSnapshot();
    expect(wrapper.text()).toContain('Edit Ban');
  });
});
