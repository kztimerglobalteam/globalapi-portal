import { shallowMount } from '@vue/test-utils';
import ServerForm from '@/components/servers/ServerForm.vue';

const createWrapper = () => shallowMount(ServerForm);

describe('New Server Form', () => {
  test('mounts properly', () => {
    const wrapper = createWrapper();
    expect(wrapper.isVueInstance()).toBeTruthy();
  });

  test('renders properly', () => {
    const wrapper = createWrapper();
    expect(wrapper.html()).toMatchSnapshot();
  });
});
