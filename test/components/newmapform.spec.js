import { shallowMount } from '@vue/test-utils';
import MapForm from '@/components/maps/NewMapForm.vue';

const createWrapper = () => shallowMount(MapForm);

describe('New Map Form', () => {
  test('mounts properly', () => {
    const wrapper = createWrapper();
    expect(wrapper.isVueInstance()).toBeTruthy();
  });

  test('renders properly', () => {
    const wrapper = createWrapper();
    expect(wrapper.html()).toMatchSnapshot();
  });
});
